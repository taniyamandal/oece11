package com.jspider.esop.repository;
import javax.websocket.server.PathParam;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.jspider.esop.allocation.Allocation;
import com.jspider.esop.entity.Employee;
import com.jspider.esop.entity.Grant;

@Repository
public interface AllocationRepository extends JpaRepository<Allocation, Long>{

	@Query("from Grant where grantNumber=:grantNumber")
	Grant getGrantByGrantNumber(@PathParam ("grantNumber") String grantNumber);


}
