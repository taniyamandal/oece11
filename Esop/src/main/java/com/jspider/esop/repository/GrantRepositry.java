package com.jspider.esop.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.jspider.esop.entity.Grant;

@Repository
public interface GrantRepositry extends JpaRepository<Grant, Long>{

}
