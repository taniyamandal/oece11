package com.jspider.esop.dto;
import java.io.Serializable;

public class GrantDto implements Serializable {

	private String grantNumber;

	private String noOfGrant;

	private String plan;

	private String employee;

	private String grantedBy;

	public String getGrantNumber() {
		return grantNumber;
	}

	public String getNoOfGrant() {
		return noOfGrant;
	}

	public String getPlan() {
		return plan;
	}

	public String getEmployee() {
		return employee;
	}

	public String getGrantedBy() {
		return grantedBy;
	}

	public void setGrantNumber(String grantNumber) {
		this.grantNumber = grantNumber;
	}

	public void setNoOfGrant(String noOfGrant) {
		this.noOfGrant = noOfGrant;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}

	public void setEmployee(String employee) {
		this.employee = employee;
	}

	public void setGrantedBy(String grantedBy) {
		this.grantedBy = grantedBy;
	}

}
