package com.jspider.esop.service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.jspider.esop.entity.Employee;
import com.jspider.esop.entity.Plan;
import com.jspider.esop.repository.EmployeeRepository;
import com.jspider.esop.repository.PlanRepository;


@Service
public class CommonService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private PlanRepository planRepository;
	private JdbcTemplate jdbcTemplate;
	
	
	public void saveEmployeeDetails(Employee employee) {
		System.out.println(employeeRepository.save(employee));
		
	}

	public List<Employee> getEmployeeDetails() {
		
		return employeeRepository.findAll();
	}

	public Employee getEmployeeByEmployeeNumber(String employeeNumber) {
		
		return employeeRepository.getEmployeeByEmployeeNumber(employeeNumber);
	}
	public void savePlanDetails(Plan plan) {
		planRepository.save(plan);
		
	}
	
public Plan getPlanByYear(String planYear) {
		
		return planRepository.getPlanByYear(planYear);
	}

}
