package com.jspider.esop.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jspider.esop.allocation.Allocation;
import com.jspider.esop.constants.AppConstant;
import com.jspider.esop.dto.GrantDto;
import com.jspider.esop.entity.Grant;
import com.jspider.esop.repository.AllocationRepository;
import com.jspider.esop.repository.GrantRepositry;

@Service
public class GrantService {

	@Autowired
	private GrantRepositry grantRepository;
	
	@Autowired
	private AllocationRepository allocationRepository;

	@Autowired
	private CommonService commonService;

	public void prepareAndUploadGrants(List<GrantDto> grantDtoList) {

		List<Grant> grantList = new ArrayList<Grant>();
		grantDtoList.forEach(each -> {
			Grant grant = new Grant();
			grant.setNoOfGrant(each.getNoOfGrant());
			grant.setGrantNumber(each.getGrantNumber());
			grant.setGrantDate(new Date());
			//grant.setAcceptedDate(each.getAcceptedDate());
			grant.setEmployee(commonService.getEmployeeByEmployeeNumber(each.getEmployee()));
			grant.setPlan(commonService.getPlanByYear(each.getPlan()));
			grant.setGrantedBy(commonService.getEmployeeByEmployeeNumber(each.getGrantedBy()));
			grant.setGrantStatus(AppConstant.STATUS);
			grant.setAccepted(false);
			grantList.add(grant);
		});
		grantRepository.saveAll(grantList);
	}
	
	public Grant getGrantByGrantNumber(String grantNumber) {
		return allocationRepository.getGrantByGrantNumber(grantNumber);
	}
	
	public void prepareAllocation(Grant grant) {
		
		int g=Integer.parseInt(grant.getEmployee().getBand());
		List<Allocation> list = new ArrayList<Allocation>();
		if(g>5) {
			Allocation allocation = new Allocation();
			allocation.setAllocationNumber(Double.parseDouble(grant.getNoOfGrant()));
			Calendar calendar = Calendar.getInstance();
			calendar.add(calendar.YEAR, 1);
			allocation.setGrantAllocationDate(calendar.getTime());
			allocation.setAllocationYear(calendar.get(Calendar.YEAR)+"");
			allocation.setGrant(grant);
			allocation.setAllocationStatus(AppConstant.STATUS);
			list.add(allocation);
		}
		else {
			Double value = Double.parseDouble(grant.getGrantNumber())/5;
			
			for(int i=0;i<5;i++) {
				Allocation allocation = new Allocation();
				allocation.setAllocationNumber(value);
				Calendar calendar = Calendar.getInstance();
				calendar.add(calendar.YEAR, i+1);
				allocation.setGrantAllocationDate(calendar.getTime());
				allocation.setAllocationYear(calendar.get(Calendar.YEAR)+"");
				allocation.setGrant(grant);
				allocation.setAllocationStatus(AppConstant.STATUS);
				list.add(allocation);
			}
			
		}
		allocationRepository.saveAll(list);
		
	}
	
	public void updateGrantStatus(String grantNumber) {
		Grant grant =getGrantByGrantNumber(grantNumber);
		prepareAllocation(grant);
	}
	
}
