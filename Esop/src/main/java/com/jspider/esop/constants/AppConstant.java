package com.jspider.esop.constants;

public interface AppConstant {

	public static String SAVE_EMPLOYEE_DETAILS = "/saveEmployeeDetails";
	
	public static String COMMON_CONTROLLER = "/";
	
	public static String GET_EMPLOYEE = "/getEmployee";
	
	public static String GET_EMPLOYEE_BY_ID = "/getEmployeeById";
	
	public static String SAVE_PLAN_DETAILS = "/savePlanDetails";
	
	public static String GET_PLAN_DETAILS = "/getPlanDetails";
	
	public static String GRANT_CONTROLLER = "/grantController";
	
	public static String UPLOAD_GRANT = "/uploadGrant";
	
	public static String GET_GRANT_BY_GRANT_NUMBER = "/getGrantByGrantNumber";
	
	public static String PREPARE_ALLOCATION = "/prepareAllocation";
	
	public static String STATUS = "pending";
	
}
