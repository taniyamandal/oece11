package com.jspider.esop.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jspider.esop.constants.AppConstant;
import com.jspider.esop.entity.Employee;
import com.jspider.esop.entity.Plan;
import com.jspider.esop.service.CommonService;

@RestController
@RequestMapping(value=AppConstant.COMMON_CONTROLLER)
public class CommonController {
	
	@Autowired
	private CommonService commonService;
	
	@PostMapping(value = AppConstant.SAVE_EMPLOYEE_DETAILS )
	public void saveMovieDetails(@RequestBody Employee employee) {
		commonService.saveEmployeeDetails(employee);
	}
	
	@GetMapping(value=AppConstant.GET_EMPLOYEE)

	public  List<Employee>  getEmployeeDetails(){
		return commonService.getEmployeeDetails();
	}
	
	@GetMapping(value=AppConstant.GET_EMPLOYEE_BY_ID)
	public Employee getEmployeeByEmployeeNumber(String employeeNumber) {
		return commonService.getEmployeeByEmployeeNumber(employeeNumber);
	}
	
	@PostMapping(value = AppConstant.SAVE_PLAN_DETAILS )
	public void savePlanDetails(@RequestBody Plan plan) {
		commonService.savePlanDetails(plan);
	}
	
	@GetMapping(value=AppConstant.GET_PLAN_DETAILS)
	public Plan getPlanByYear(String planYear) {
		return commonService.getPlanByYear(planYear);
	}

}
