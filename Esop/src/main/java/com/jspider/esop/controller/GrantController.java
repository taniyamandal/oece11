package com.jspider.esop.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jspider.esop.constants.AppConstant;
import com.jspider.esop.dto.GrantDto;
import com.jspider.esop.entity.Grant;
import com.jspider.esop.service.GrantService;




@RestController
@RequestMapping(value=AppConstant.GRANT_CONTROLLER)
public class GrantController {
	
	@Autowired
	private GrantService grantService;
	
	@PostMapping(value = AppConstant.UPLOAD_GRANT)
	public void prepareGrants(@RequestBody List<GrantDto> list) {

		grantService.prepareAndUploadGrants(list);
	}
	
	@PostMapping(value = AppConstant.GET_GRANT_BY_GRANT_NUMBER)
	public Grant getGrantByGrantNumber(@RequestBody String grantNumber) {
		return grantService.getGrantByGrantNumber(grantNumber);
		
	}

	@PostMapping(value = AppConstant.PREPARE_ALLOCATION)
	public void updateGrantStatus(@RequestBody String grantNumber) {
		 grantService.updateGrantStatus(grantNumber);
		
	}
}
