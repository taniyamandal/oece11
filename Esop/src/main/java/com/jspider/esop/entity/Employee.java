package com.jspider.esop.entity;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="employee_details")
public class Employee implements Serializable{
	
	@Id
	@GenericGenerator(name = "auto", strategy = "increment")
	@GeneratedValue(generator = "auto")
	
	@Column(name="empId")
	private Long empId;
	
	@Column(name="emp_name")
	private String empName;
	
	@Column(name="emp_number")
	private String employeeNumber;
	
	@Column(name="band")
	private String band;
	
	@Column(name="contact_number")
	private String contactNumber;
	
	@Column(name="city")
	private String city;
	
	@Column(name="department")
	private String department;
	
	@Column(name="email")
	private String email;

	public Long getEmpId() {
		return empId;
	}

	public String getEmpName() {
		return empName;
	}

	public String getEmployeeNumber() {
		return employeeNumber;
	}

	public String getBand() {
		return band;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public String getCity() {
		return city;
	}

	public String getDepartment() {
		return department;
	}

	public String getEmail() {
		return email;
	}

	public void setEmpId(Long empId) {
		this.empId = empId;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public void setEmployeeNumber(String employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public void setBand(String band) {
		this.band = band;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", empName=" + empName + ", employeeNumber=" + employeeNumber + ", band="
				+ band + ", contactNumber=" + contactNumber + ", city=" + city + ", department=" + department
				+ ", email=" + email + "]";
	}
	

}
