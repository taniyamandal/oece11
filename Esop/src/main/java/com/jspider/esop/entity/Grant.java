package com.jspider.esop.entity;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="grant_info")
public class Grant implements Serializable{

	@Id
	@GenericGenerator(name = "auto", strategy = "increment")
	@GeneratedValue(generator = "auto")
	@Column(name = "id")
	private Long id;
	
	@Column(name = "grant_number")
	private String grantNumber;
	
	@Column(name = "no_of_grant")
	private String noOfGrant;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="plan_id")
	private  Plan plan;

	@Column(name = "grant_date")
	private Date grantDate;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="employee_id")
	private Employee employee;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="granted_by")
	private Employee grantedBy;
	
	@Column(name = "grant_status")
	private String grantStatus;
	
	@Column(name = "accepted")
	private Boolean accepted;
	
	@Column(name = "accepted_date")
	private String acceptedDate;

	public Long getId() {
		return id;
	}

	public String getGrantNumber() {
		return grantNumber;
	}

	public String getNoOfGrant() {
		return noOfGrant;
	}

	public Plan getPlan() {
		return plan;
	}

	public Date getGrantDate() {
		return grantDate;
	}

	public Employee getEmployee() {
		return employee;
	}

	public Employee getGrantedBy() {
		return grantedBy;
	}

	public String getGrantStatus() {
		return grantStatus;
	}

	public Boolean getAccepted() {
		return accepted;
	}

	public String getAcceptedDate() {
		return acceptedDate;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setGrantNumber(String grantNumber) {
		this.grantNumber = grantNumber;
	}

	public void setNoOfGrant(String noOfGrant) {
		this.noOfGrant = noOfGrant;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public void setGrantDate(Date grantDate) {
		this.grantDate = grantDate;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public void setGrantedBy(Employee grantedBy) {
		this.grantedBy = grantedBy;
	}

	public void setGrantStatus(String grantStatus) {
		this.grantStatus = grantStatus;
	}

	public void setAccepted(Boolean accepted) {
		this.accepted = accepted;
	}

	public void setAcceptedDate(String acceptedDate) {
		this.acceptedDate = acceptedDate;
	}

	@Override
	public String toString() {
		return "Grant [id=" + id + ", grantNumber=" + grantNumber + ", noOfGrant=" + noOfGrant + ", plan=" + plan
				+ ", grantDate=" + grantDate + ", employee=" + employee + ", grantedBy=" + grantedBy + ", grantStatus="
				+ grantStatus + ", accepted=" + accepted + ", acceptedDate=" + acceptedDate + "]";
	}
	
}
