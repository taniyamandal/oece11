package com.jspider.esop.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="plan")
public class Plan implements Serializable{
	
	@Id
	@GenericGenerator(name = "auto", strategy = "increment")
	@GeneratedValue(generator = "auto")
	
	@Column(name="id")
	private Long Id;
	
	@Column(name="plan_year")
	private String planYear;
	
	@Column(name="plan_for_date")
	private String planForDate;
	
	@Column(name="plan_end_date")
	private String planEndDate;

	public Long getId() {
		return Id;
	}

	public String getPlanYear() {
		return planYear;
	}

	public String getPlanForDate() {
		return planForDate;
	}

	public String getPlanEndDate() {
		return planEndDate;
	}

	public void setId(Long id) {
		Id = id;
	}

	public void setPlanYear(String planYear) {
		this.planYear = planYear;
	}

	public void setPlanForDate(String planForDate) {
		this.planForDate = planForDate;
	}

	public void setPlanEndDate(String planEndDate) {
		this.planEndDate = planEndDate;
	}

	@Override
	public String toString() {
		return "Plan [Id=" + Id + ", planYear=" + planYear + ", planForDate=" + planForDate + ", planEndDate="
				+ planEndDate + "]";
	}
	

}
