package com.jspider.esop.allocation;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import com.jspider.esop.entity.Grant;


@Entity
@Table(name="allocation")
public class Allocation implements Serializable{
	@Id
	@GenericGenerator(name = "auto", strategy = "increment")
	@GeneratedValue(generator = "auto")
	
	@Column(name = "id")
	private Long id;
	
	@Column(name = "allocation_status")
	private String allocationStatus;
	
	@Column(name = "allocation_number")
	private Double allocationNumber;
	
	@Column(name = "allocation_year")
	private String allocationYear;
	
	@Column(name = "grant_allocation_date")
	private Date grantAllocationDate;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="granted_id")
	private  Grant grant;

	
	public Double getAllocationNumber() {
		return allocationNumber;
	}

	public Date getGrantAllocationDate() {
		return grantAllocationDate;
	}

	public Grant getGrant() {
		return grant;
	}

	public void setAllocationNumber(Double allocationNumber) {
		this.allocationNumber = allocationNumber;
	}

	public void setGrantAllocationDate(Date grantAllocationDate) {
		this.grantAllocationDate = grantAllocationDate;
	}

	public void setGrant(Grant grant) {
		this.grant = grant;
	}

	public Long getId() {
		return id;
	}

	public String getAllocationStatus() {
		return allocationStatus;
	}

	
	public String getAllocationYear() {
		return allocationYear;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAllocationStatus(String allocationStatus) {
		this.allocationStatus = allocationStatus;
	}

	public void setAllocationYear(String allocationYear) {
		this.allocationYear = allocationYear;
	}

	@Override
	public String toString() {
		return "Allocation [id=" + id + ", allocationStatus=" + allocationStatus + ", allocationNumber="
				+ allocationNumber + ", allocationYear=" + allocationYear + ", grantAllocationDate="
				+ grantAllocationDate + ", grant=" + grant + "]";
	}

	
	
}
