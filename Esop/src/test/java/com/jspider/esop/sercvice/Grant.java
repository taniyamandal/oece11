
package com.jspider.esop.sercvice;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.jspider.esop.repository.GrantRepositry;
import com.jspider.esop.service.GrantService;

@RunWith(value = PowerMockRunner.class)
public class Grant {

	@Mock
	private GrantRepositry grantRepositry;
	
	@InjectMocks
	@Spy
	private GrantService grantServiceSpy;
	
	@Test
	public void testGetGrant() throws Exception{
		MockitoAnnotations.initMocks(this);
		Grant  grantMock = PowerMockito.mock(Grant.class);
		PowerMockito.when(grantRepositry,"getGrantByGrantNumber",anyString()).thenReturn(grantMock);
		ReflectionTestUtils.invokeMethod(grantServiceSpy,"getGrantByGrantNumber","grantNumber");
		assertTrue(true);
	}

	
}
